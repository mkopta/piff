#include <stdio.h>
#define MAXRADEK 1000

int readline(char *s, int lim) {
  int c, i = 0;
  while (--lim > 0 && (c = getchar()) != EOF && c != '\n')
    s[i++] = c;
  if (c == '\n')
    s[i++] = c;
  s[i] = '\0';
  return i;
}

/* find last occurence of s in r and return index */
int strfindl(char *r, char *s) {
  int i, j, k, index = -1;
  for (i = 0; r[i] != '\0'; i++) {
    for (j = i, k = 0; s[k] != '\0' && r[j] == s[k]; j++, k++);
    if (k > 0 && s[k] == '\0')
      index = i;
  }
  return index;
}

int main(int argc, char **argv) {
  char *pattern;
  char line[MAXRADEK];
  int index;
  if (argc != 2) {
    printf("Usage: %s <pattern>\n", *argv);
    return 0;
  } else {
    pattern = *++argv;
  }
  while (readline(line, MAXRADEK) > 0)
    if ((index = strfindl(line, pattern)) >= 0)
      printf("%d, %s", index, line);
  return 0;
}
